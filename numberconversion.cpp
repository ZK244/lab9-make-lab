#include "numberconversion.h"

int roman_char(char r)
{
    switch (r)
    {
        case 'M': return 1000;
        case 'D': return 500;
        case 'C': return 100;
        case 'L': return 50;
        case 'X': return 10;
        case 'V': return 5;
        case 'I': return 1;
    default: return 0;
    }
}

int romantoint(std::string roman_numeral)
{
  int result = 0, current = 0;

    for (int i = 0; i < roman_numeral.size(); i++) {
        current = roman_char(roman_numeral[i]);

        if (current < roman_char(roman_numeral[i + 1]))
            result -= current;
        else
            result += current;
    }

    return result;

}

std::string inttoroman(int number)
{
  std::string romans[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int values[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

	std::string result = "";

        for (int i = 0; i<13; i++)
        {
            while(number - values[i] >= 0)
            {
                result += romans[i];
                number -= values[i];
            }
        }

        return result;
}
